<?php

class MainController extends AdminController
{
	public function actionIndex()
	{
		$this->layout = 'admin/admin_column2';
		$this->render('index', array(

		));
	}
	
	public function actionToggleDummy()
	{
		$model = Config::model()->resetScope()->findByPk('dummy');
		if ($model === null)
		{
			$model = new Config('string_param');
			$model->scenario = 'string_param';
			$model->id = 'dummy';
			$model->key = 'Сайт в разработке?';
			$model->value = '1';
			$model->is_file = 0;
			$model->is_hidden = 1;
		}
		else
		{
			$model->value = (int) !$model->value;
		}	

		if ($model->save())
		{
			$text = !$model->value ? 'Заглушка выключена' : 'Заглушка выключена';
			Yii::app()->user->setFlash('result', $text);
			$this->redirect(array('index'));
		}
		else
		{
			throw new CHttpException(500, 'Что-то пошло не так');
		}
	}

	public function actionImageUpload()
	{
		$dir = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'redactor' . DIRECTORY_SEPARATOR;
		
		if (!is_dir($dir))
		{
			if (!mkdir($dir, 777, true))
				throw new CHttpException(500, 'Unable to create directory');
		}

		if (isset($_FILES['file']['type']))
		{
			$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

			if (
				$_FILES['file']['type'] == 'image/png'
			 || $_FILES['file']['type'] == 'image/jpg'
			 || $_FILES['file']['type'] == 'image/gif'
			 || $_FILES['file']['type'] == 'image/jpeg'
			 || $_FILES['file']['type'] == 'image/pjpeg'
			)
			{
				// setting file's mysterious name
				$filename = md5(date('YmdHis')).'.jpg';

				if (!is_dir($dir))
				{
					mkdir($dir);
				}
				
				// copying
				move_uploaded_file($_FILES['file']['tmp_name'], $dir . $filename);

				// displaying file
				$array = array(
					'filelink' => Yii::app()->baseUrl . '/uploads/redactor/' . $filename
				);

				echo stripslashes(json_encode($array));
			}	
		}
		else
			throw new CHttpException(400);
	}
}