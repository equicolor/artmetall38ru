<?php

class MediaController extends AdminController
{
	public $defaultAction = 'admin';
	
	// public function actionIndex()
	// {
	// 	$mediaDp = new CActiveDataProvider('Media', array(
	// 		'criteria' => array(
	// 			'scopes' => 'exceptPictures',
	// 			'with' => array(
	// 				'items',
	// 			),
	// 		),
	// 		'pagination' => false,
	// 	));
		
	// 	Yii::app()->theme = null;
		
	// 	$this->render('index', array(
	// 		'mediaDp' => $mediaDp,
	// 	));
	// }
	
	// public function actionView($id)
	// {
		// $this->render('view',array(
			// 'model'=>$this->loadModel($id),
		// ));
	// }

	public function actionCreatePicture($id)
	{
		$album = $this->loadModel($id, Media::TYPE_ALBUM); 

		$model = new Picture;

		if(isset($_POST['Picture']))
		{
			$model->attributes=$_POST['Picture'];
			
			$model->parent_id = $album->id;
			
			if($model->save())
			{				
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				
				if ($model->parent_id === null)
				{
					$this->redirect(array('admin'));
				}
				else
				{
					$this->redirect(array('updateAlbum', 'id' => $model->parent_id));
				}
			}
		}

		$this->render('create_picture',array(
			'model' => $model,
			'album' => $album,
		));
	}
	
	// public function actionCreateVideo()
	// {
	// 	$model = new Video;

	// 	if(isset($_POST['Video']))
	// 	{
	// 		$model->attributes=$_POST['Video'];
	// 		if($model->save())
	// 		{				
	// 			Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
	// 			$this->redirect(array('admin'));
	// 		}
	// 	}

	// 	$this->render('create_video',array(
	// 		'model'=>$model,
	// 	));
	// }

	public function actionCreateAlbum()
	{
		$model = AlbumForm::create();
		
		if ($model->issetAttributes($_POST))
		{
			$model->safeAssign($_POST);
			
			if ($model->save())
			{				
				Yii::app()->user->setFlash('result', 'Альбом создан');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create_album',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$class = get_class($model);
		
		if(isset($_POST[$class]))
		{
			$model->attributes=$_POST[$class];
			if($model->save())
			{			
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionAdmin()
	{
		$model=new Media('search');
		
		$model->exceptPictures();
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Media']))
			$model->attributes=$_GET['Media'];
			
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	/* bad */
	public function actionUpdateAlbum($id)
	{
		$album = $this->loadModel($id, Media::TYPE_ALBUM);
		
		$model = new Media('search');
		
		$model->exceptAlbums()->parent($id);
		
		$model->unsetAttributes(); 
		if(isset($_GET['Media']))
			$model->attributes=$_GET['Media'];
			
		$this->render('update_album',array(
			'model' => $model,
			'album' => $album,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Media the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id, $type = null)
	{
		if ($type === null)
		{
			$model = Media::model()->findByPk($id);
		}
		else
		{
			$model = Media::model()->type($type)->findByPk($id);
		}
		
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Media $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='media-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
