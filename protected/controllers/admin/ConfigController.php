<?php

class ConfigController extends AdminController
{
	public $defaultAction = 'admin';
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Config;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$fileForm = isset($_GET['file']) ? $_GET['file'] == true : false;
		$model->scenario = $fileForm ? 'file_param' : 'string_param';
		
		if(isset($_POST['Config']))
		{
			$model->attributes=$_POST['Config'];
			if ($model->save())
			{
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				if (isset($_POST['apply']))
				{
					$this->redirect(array('update', 'id' => $model->id));
				}
				else
				{
					$this->redirect(array('admin'));
				}
			}
		}

		
		$this->render('create',array(
			'model'=>$model,
			'fileForm' => $fileForm,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = $model->is_file ? 'file_param' : 'string_param';
			
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Config']))
		{
			$model->attributes=$_POST['Config'];
			if ($model->save())
			{
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				if (isset($_POST['apply']))
				{
					$this->refresh();
				}
				else
				{
					$this->redirect(array('admin'));
				}
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Config('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Config']))
			$model->attributes=$_GET['Config'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Config the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Config::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Config $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='config-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	

	public function actionPassword()
	{
		$model = Config::model()->resetScope()->findByPk('password');
		
		if ($model === null)
			throw new CHttpException(404);
		
		$model->scenario = 'string_param';
		
		if(isset($_POST['Config']))
		{
			$model->attributes=$_POST['Config'];
			if ($model->save())
			{
				Yii::app()->user->setFlash('result', 'Изменения успешно сохранены');
				$this->refresh();
			}
		}
		
		$model->value = '';
		
		$this->render('password',array(
			'model'=>$model,
		));
	}
}
