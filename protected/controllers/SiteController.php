<?php

class SiteController extends Controller
{
	public $layout = 'main';

	// public $defaultAction =  'dummy';
	
	/**
	 * Declares class-based actions.
	 */
	/*public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}*/

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

	public function actionIndex()
	{
		if (Config::value('dummy'))
			$this->redirect(array('dummy'));
		
		$services = Service::model()->findAll();

		$this->render('index', array(
			'services' => $services,
		));
	}

	public function actionDummy()
	{
		$this->render('dummy');
	}

	public function actionService()
	{
		$this->layout = 'info';
		
		$models = Service::model()->findAll();

		$this->render('services', array(
			'models' => $models,
			'feedback' => new FeedbackForm,
		));
	}

	public function actionGallery()
	{
		$this->layout = 'info';

		$models = Project::model()->with(array('media', 'media.items'))->findAll();

		$this->render('gallery', array(
			'models' => $models,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		// $model=new ContactForm;

		// if (isset($_POST['ContactForm']))
		// {
		// 	$model->attributes=$_POST['ContactForm'];
		// 	$model->send();
		// }

		// $this->redirectBack('site/index');

		$this->layout = 'info';
		$this->render('contact');
	}

	public function actionFeedback()
	{
		if (!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(400);
		
		$model = new FeedbackForm;
		
		if (isset($_POST['FeedbackForm']))
		{
			$model->attributes = $_POST['FeedbackForm'];

			$valid = $model->validate();

			if ($valid)
			{
				if ($model->send())
				{
					echo CJSON::encode(array(
						'status' => 'success',
					));
				}
				else
					throw new CHttpException(500);				
				
				Yii::app()->end();
			}

			echo CActiveForm::validate($model);
		}	
		else
			throw new CHttpException(400);
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form

		$this->layout = 'admin/login';
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}