<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/admin'); ?>
	<div class="span8 well">	

		<?php if (Yii::app()->user->hasFlash('result')): ?>
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?= Yii::app()->user->getFlash('result'); ?>
			</div>
		<?php endif; ?>
		
		<div class="page-header">
			<h3><?= $this->pageTitle ?></h3>
		</div>

		<?= $content; ?>
	</div><!--/span-->
	<div class="span2 well sidebar-nav">
		<?php $this->widget('bootstrap.widgets.TbNav', array(
			'type' => TbHtml::NAV_TYPE_LIST,
			'items' => array_merge(array(array('label' => 'Операции')), $this->menu),
		))?>
	</div>
<?php $this->endContent(); ?>