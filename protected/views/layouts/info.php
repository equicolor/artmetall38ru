<!DOCTYPE html>
<html>

<head>
	<base href="<?= Yii::app()->getBaseUrl(true); ?>/"/>
	<meta charset="utf-8" />
	<title>ArtMetall - Услуги</title>
	<link rel="stylesheet" href="style.css" />
	<!--?php Yii::app()->clientScript->registerCoreScript('jquery'); ?-->

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.js"></script> <!-- 16 KB -->
</head>

<body>
	<div class="page-wrapper">
		<div class="b-header">
			<div class="b-header--content">
				<a href="<?= Yii::app()->createUrl('site/index'); ?>"><i class="s s--header_logo"></i></a>

				<div class="header--contacts">
					<span class="header__telephone"><a href="tel:<?= Config::value('phone', array('Config', 'parseTel')); ?>"><?= Config::value('phone'); ?></a></span>
					<a href="mailto:<?= Config::value('email'); ?>" class="header__mail"><?= Config::value('email'); ?></a>
				</div>
			</div>
		</div>

		<div class="menu">
			<?php $this->widget('zii.widgets.CMenu', array(
				'htmlOptions' => array(
					'class' => 'b-menu',
				),
				'items' => array(
					array('label' => 'Главная', 'url' => array('site/index')),
					array('label' => 'Услуги', 'url' => array('site/service')),
					array('label' => 'Портфолио', 'url' => array('site/gallery')),
					array('label' => 'Контакты', 'url' => array('site/contact')),
				),
			)); ?>
		</div>

		<div class="b-content">
			<?= $content; ?>
		</div>

		<div class="page-buffer"></div>
	</div>

	<div class="page-footer">
		<div class="b-footer">
			<div class="page__layout">
				<ul class="b-main--footer">
					<li><p class="b-main--footer_copyright">© <?= date('Y');?> ArtMetall</p></li>
					<li><p class="b-main--footer_address"><?= Config::value('address'); ?></p></li>
					<li><p class="b-main--footer_telephone"><?= Config::value('phone'); ?></p></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="popup">
		<span class="popup__text">Спасибо за ваш вопрос! Скоро мы с вами свяжемся!</span>
	</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter28139490 = new Ya.Metrika({id:28139490,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28139490" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>

</html>