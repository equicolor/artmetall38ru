<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="b-error">
	<h2><?php echo $code; ?></h2>

	<div class="error">
	<?php echo CHtml::encode($message); ?>
	</div>

	<a href="<?= $this->createUrl('site/index'); ?>" class="b-error__link">Вернуться на главную страницу</a>
</div>