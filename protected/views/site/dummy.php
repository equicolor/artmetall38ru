		<i class="s s--logo"></i>

	
			<style>
				.b-dummy {
				  	-webkit-font-smoothing: antialiased;
					top: 65%; /* Отступ в процентах от верхнего края окна */
					/*left: 50%;  Отступ в процентах от левого края окна */
					/*width: 600px;  Ширина блока */
					width: 100%;
					height: 600px; /* Высота блока */
					position: absolute; /* Абсолютное позиционирование блока */
					margin-top: -160px; /* Отрицательный отступ от верхнего края страницы, должен равняться половине высоты блока со знаком минус */
					/*margin-left: -200px;  Отрицательный отступ от левого края страницы, должен равняться половине высоты блока со знаком минус */
					text-align: center;
					color: #FFFFFF;
					z-index: 100;
				}
				.b-dummy p
				{
					font-size: 34px;
				}
				.b-dummy p a
				{
					color: #AAA;
					text-decoration: none;
					
				}
				.b-dummy p a:hover
				{
					color: #FFF;
				}
			</style>

			<div class="b-dummy">
				<p>
					Извините, сайт в разработке.
				</p>
				<p>&nbsp;</p>
				<p>
					Если у вас есть вопросы, звоните <a href="tel:89025775634">8 (902) 577-56-34</a>
				</p>
			</div>
