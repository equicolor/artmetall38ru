	<?php foreach ($models as $model): ?>
		<ul class="b-service--items">
			<li>
				<div class="b-service--items_imagewrapper">
					<!-- <img src="images/service_1.jpg"> -->
					<?= CHtml::image($model->image->thumb('logo')); ?>
				</div>

				<h3>Заказчик: <?= $model->name; ?></h3>

				<?= $model->description; ?>

				<div style="clear: both;"></div>

				<div class="b-service--items__content" style="display: none;">
					<?= $model->content; ?>
				</div>

				<div class="fotorama fotorama_big" data-width="100%" data-height="400" data-fit="cover" data-nav="thumbs" data-allowfullscreen="true">
				<?php foreach ($model->media->items as $item): ?>
					<?= CHtml::image($item->content); ?>
				<?php endforeach; ?>
				</div>

				<button class="b-service--items_button">Открыть подробное описание</button>
			</li>
		</ul>
	<?php endforeach; ?>

<script type="text/javascript">
	$( ".b-service--items li .b-service--items_button" ).click(function() {
		if ($(this).parent('li').find('.fotorama_big').hasClass("active_slider")) {
			$(this).parent('li').find('.b-service--items_button').text('Открыть подробное описание');
			$(this).parent('li').find('.b-service--items__content').slideUp();
			$(this).parent('li').find('.fotorama_big').slideUp();
			$(this).parent('li').find('ul').slideUp();
			$(this).parent('li').find('.fotorama_big').removeClass("active_slider");
		} else {
			$(this).parent('li').find('.b-service--items_button').text('Закрыть подробное описание');
			$(this).parent('li').find('ul').slideDown();
			$(this).parent('li').find('.fotorama_big').slideDown();
			$(this).parent('li').find('.b-service--items__content').slideDown();
			$(this).parent('li').find('.fotorama_big').addClass("active_slider");
		};
 	});
 </script>