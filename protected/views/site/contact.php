<b>Наш адрес:</b>
<p><?= Config::value('address'); ?></p>

<b>Контактный телефон:</b>
<p><?= Config::value('phone'); ?></p>

<b>Контактный e-mail</b><br />
<a href="mailto:<?= Config::value('email'); ?>"><?= Config::value('email'); ?></a>

<div style="display: block; height: 30px;"></div>

<?php $this->renderPartial('_feedback', array(
	'i' => 0,
	'feedback' => new FeedbackForm,
)); ?>

<div style="display: block; height: 30px;"></div>

<a id="firmsonmap_biglink" href="http://maps.2gis.ru/#/?history=project/irkutsk/center/104.30507449451,52.277815150148/zoom/18/state/widget/id/1548640653103660/firms/1548640653103660">Перейти к большой карте</a>
<script charset="utf-8" type="text/javascript" src="http://firmsonmap.api.2gis.ru/js/DGWidgetLoader.js"></script>
<script charset="utf-8" type="text/javascript">new DGWidgetLoader({"borderColor":"#a3a3a3","width":"100%","height":"450","wid":"b1afc0e8daee8e8459ed939f37aec5a7","pos":{"lon":"104.30507449451","lat":"52.277815150148","zoom":"18"},"opt":{"ref":"hidden","card":["name","contacts","schedule","payings"],"city":"irkutsk"},"org":[{"id":"1548640653103660","hash":"ge4iqa34691IJ1HJf1g7uvgc43660J34be88578834068658723e69G41J0I99669f"}]});</script>
<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>