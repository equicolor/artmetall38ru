
				<!-- must be modal window -->

				<?php $form = $this->beginWidget('CActiveForm', array(
					'id' => 'feedback-form-' . $i,
					'htmlOptions' => array(
						'class' => 'b-service--backform',
					),
					'enableClientValidation' => true,
					// 'enableAjaxValidation' => true,
					'clientOptions' => array(
						'validateOnSubmit' => true,
						'validateOnChange' => false,
						'validateOnInput' => false,
						// 'hideErrorMessage' => true,
					),
				)); ?>

					<div>
						<?= $form->textField($feedback, 'theme', array(
							'placeholder' => $feedback->getAttributeLabel('theme'),
						)); ?>
						<?= $form->error($feedback, 'theme');?>
					</div>

					<div>
						<?= $form->textField($feedback, 'email', array(
							'placeholder' => $feedback->getAttributeLabel('email'),
						)); ?>
						<?= $form->error($feedback, 'email');?>
					</div>

					<div>
						<?= $form->textField($feedback, 'phone', array(
							'placeholder' => $feedback->getAttributeLabel('phone'),
						)); ?>
						<?= $form->error($feedback, 'phone');?>
					</div>

					<div>
						<?= $form->textArea($feedback, 'body', array(
							'placeholder' => $feedback->getAttributeLabel('body'),
						)); ?>
						<?= $form->error($feedback, 'body');?>
					</div>

					<?= CHtml::ajaxSubmitButton('Отправить', $this->createUrl('site/feedback'), array(
						'type' => 'POST',
						'dataType' => 'json',
						'success' =>
<<<JS
function(data) {
	if(data.status=="success")
	{
		$(".popup").slideDown();
        $("#feedback-form-{$i}")[0].reset();
        $(".popup").delay(2000).slideUp();
	}
	else
	{
		$.each(data, function(key, val) 
		{
			$("#feedback-form-{$i} #"+key+"_em_").text(val);
			$("#feedback-form-{$i} #"+key+"_em_").show();
		});
	}
}
JS
						,
						'error' => 'function () {alert(\'nope\')}',
					), array(
							'class' => 'b-service--submit',
					)); ?>

				<?php $this->endWidget(); ?>
				
				<!-- must be modal window -->