		<i class="s s--logo"></i>

		<span class="b-main--text">
			<?php foreach ($services as $service): ?>
				<!-- Металлокострукции<br />Металлоизделия<br />Плазменная резка<br />Портальные станки с ЧПУ<br />Водосливные системы и доборные элементы -->
				<a href="<?= $this->createUrl('service'); ?>#service_<?= $service->alias; ?>"><?= $service->name; ?></a><br />
			<?php endforeach; ?>
		</span>

		<div class="b-main--links-block">
			<a href="<?= Yii::app()->createUrl('site/service'); ?>" class="b-main--links">услуги</a>
			<a href="<?= Yii::app()->createUrl('site/gallery'); ?>" class="b-main--links">галерея</a>
			<a href="<?= Yii::app()->createUrl('site/contact'); ?>" class="b-main--links">контакты</a>
		</div>

		<ul class="b-main--footer">
			<li><p class="b-main--footer_copyright">© <?= date('Y');?> ArtMetall</p></li>
			<li><p class="b-main--footer_address"><?= Config::value('address'); ?></p></li>
			<li><p class="b-main--footer_telephone"><?= Config::value('phone'); ?></p></li>
		</ul>