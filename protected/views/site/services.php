		<ul class="b-service--anchor">
			<?php foreach ($models as $model): ?>
			<li>
				<a href="<?= Yii::app()->request->url; ?>#service_<?= $model->alias; ?>">
					<div class="b-service--anchor_imagewrapper">
						<?= CHtml::image($model->image->thumb('menu')); ?>
					</div>
					<p><?= $model->name; ?></p>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
		<ul class="b-service--items">
			<?php $i = 0; ?>
			<?php foreach ($models as $model): ?>

			<li>
				<h2 id="service_<?= $model->alias; ?>"><?= $model->name; ?></h2>

				<?php if ($model->media !== null): ?>
				<div class="b-service--items_imagewrapper">
					<div class="fotorama" data-width="100%" data-height="250px" data-fit="cover" style="border-radius: 5px;">
						<?php foreach ($model->media->items as $image): ?>
							<?= CHtml::image($image->content); ?>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>

				<div class="b-service--content">
					<?= $model->description; ?>
				</div>

				<div style="clear: both;"></div>

				<div class="b-service--detailed">
					<?= $model->content; ?>
					<?php foreach ($model->albums as $album): ?>
						<div class="b-service--fotorama">
							<div class="fotorama" data-width="100%" data-height="250px" data-fit="cover" data-allowfullscreen="true">
								<?php foreach ($album->items as $image): ?>
									<?= CHtml::image($image->content); ?>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>

				<div style="clear: both;"></div>

				<button class="b-service--items_button service">Открыть подробное описание</button>

				<div style="clear: both;"></div>
			</li>
			<?php endforeach; ?>
		</ul>

		<script type="text/javascript">
		 	$('.b-service--items').on('click', '.service', function(evt) {
		 		if ($(this).parent('li').find('.b-service--detailed').hasClass("active_slider")) {

					$(this).parent('li').find('.b-service--items_button').text('Открыть подробное описание');
					$(this).parent('li').find('.b-service--detailed').slideUp();
					$(this).parent('li').find('.b-service--fotorama').slideUp();
					$(this).parent('li').find('ul').slideUp();
					$(this).parent('li').find('.b-service--detailed').removeClass("active_slider");

				} else {

					$(this).parent('li').find('.b-service--items_button').text('Закрыть подробное описание');
					$(this).parent('li').find('ul').slideDown();
					$(this).parent('li').find('.b-service--fotorama').slideDown();
					$(this).parent('li').find('.b-service--detailed').slideDown();
					$(this).parent('li').find('.b-service--detailed').addClass("active_slider");

				};
		 	});
		 </script>
