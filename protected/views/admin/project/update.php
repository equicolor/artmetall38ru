<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Портфолио'=>array('admin'),
	$model->project->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Список проектов', 'url'=>array('admin')),
);

$this->pageTitle = "Редактирование проекта {$model->project->name}";
?>

<h4>Галерея</h4>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'media-grid',
	'dataProvider'=>$images,
	// 'filter'=>false,
	'columns'=>array(	
		'id',
		'name',
		array(
			'name' => 'content',
			'type' => 'raw',
			'value' => 'CHtml::image($data->content, "", array("style" => "height: 100px"))',
		),
		array(
			'name' => 'type',
			'value' => '$data->typeName',
		),
		'createTime:datetime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
			'updateButtonUrl' => 'Yii::app()->createUrl("admin/media/update", array("id" => $data->id))',
			'deleteButtonUrl' => 'Yii::app()->createUrl("admin/media/delete", array("id" => $data->id))',
		),
	),
)); ?>

<h4>Проект</h4>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
