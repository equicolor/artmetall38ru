<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Портфолио'=>array('admin'),
	'Список проектов',
);

$this->menu=array(
	array('label'=>'Создать проект', 'url'=>array('create')),
);

$this->pageTitle = 'Портфолио: список проектов';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'service-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'rowCssClassExpression'=>'"Project[]_{$data->id}"',
	'afterAjaxUpdate' => 'function(id, data){initSortable()	}',
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'viewButtonUrl' => 'Yii::app()->createUrl(\'site/gallery\');'
		),
	),
)); ?>

<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">×</button>
	В таблице работает сортировка с помощью Drag&Drop.
</div>

<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerScript('dragndrop_section_sorting', 
<<<JS
	$(function() {
		var fixHelper = function(e, ui) 
		{
            ui.children().each(
				function() 
				{
					$(this).width($(this).width());
				}
			);
			
			return ui;
        };
		
		$( "#service-grid tbody").sortable({
			helper: fixHelper,
			update: function(event, ui) {
				var info = $(this).sortable("serialize", {
					key : 'Project[]',
					attribute: 'class',
				});
				
				$.ajax({
					type: "POST",
					url: "{$this->createUrl('sort')}",
					data: info,
					context: document.body,
				});
			},
		}).disableSelection();
		
	});
JS
); ?>