<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Портфолио'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать проект';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>