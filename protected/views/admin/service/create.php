<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Услуги'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список услуг', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать услугу';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>