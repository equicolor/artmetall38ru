<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Услуги'=>array('admin'),
	$model->service->name,
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать услугу', 'url'=>array('create')),
	array('label'=>'Список услуг', 'url'=>array('admin')),
);

$this->pageTitle = "Редактирование услуги {$model->service->name}";
?>

<h4>Галерея</h4>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'media-grid',
	'dataProvider'=>$images,
	// 'filter'=>false,
	'columns'=>array(	
		'id',
		'name',
		array(
			'name' => 'content',
			'type' => 'raw',
			'value' => 'CHtml::image($data->content, "", array("style" => "height: 100px"))',
		),
		array(
			'name' => 'type',
			'value' => '$data->typeName',
		),
		'createTime:datetime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
			'updateButtonUrl' => 'Yii::app()->createUrl("admin/media/update", array("id" => $data->id))',
			'deleteButtonUrl' => 'Yii::app()->createUrl("admin/media/delete", array("id" => $data->id))',
		),
	),
)); ?>

<h4>Услуга</h4>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>