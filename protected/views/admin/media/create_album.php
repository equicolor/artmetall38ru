<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив'=>array('admin'),
	'Создать альбом',
);

$this->menu=array(
	array('label'=>'Медиа-архив', 'url'=>array('admin')),
	// array('label'=>'Создать видео', 'url'=>array('createVideo')),
	// array('label'=>'Создать изображение', 'url'=>array('createPicture')),
	array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);

$this->pageTitle = 'Создать альбом';
?>

<?php $this->renderPartial('_albumform_form', array('model'=>$model)); ?>