<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив' => array('admin'),
	'Альбом ' . $album->name,
);

$this->menu=array(
	array('label'=>'Добавить изображение', 'url'=>array('createPicture', 'id' => $album->id)),
	array('label'=>'Медиа-архив', 'url'=>array('admin')),
	// array('label'=>'Создать видео', 'url'=>array('createVideo')),
	array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);

$this->pageTitle = "Альбом {$album->name}";
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'media-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>false,
	'columns'=>array(
		'id',		
		array(
			'name' => 'content',
			'type' => 'raw',
			'value' => 'CHtml::image($data->content, "", array("style" => "height: 100px"))',
		),
		// 'name',
		// array(
		// 	'name' => 'type',
		// 	'value' => '$data->typeName',
		// ),
		'createTime:datetime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
