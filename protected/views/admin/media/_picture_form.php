<?php
/* @var $this MediaController */
/* @var $model Media */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'media-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'	
	),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>
	
	<?php echo $form->errorSummary($model); ?>

	<?= $form->fileFieldControlGroup($model, 'content'); ?>
	<?= CHtml::image($model->content, "", array("style" => "height: 100px")) ?>
	
	<?//= $form->textFieldControlGroup($model, 'name', array('class' => 'span8', 'maxLength' => 255)) ?>
	
	<?//= $form->textAreaControlGroup($model, 'description', array('rows' => 6, 'class' => 'span8')) ?>
	
	<?//= $form->dropdownListRow($model, 'parent_id', CHtml::listData(Album::model()->findAll(), 'id', 'id'), array('class' => 'span8', 'empty' => '')) ?>

	<div class="form-actions">
		<?=	TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>
