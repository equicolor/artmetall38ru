<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив'=>array('admin'),
	'Создать изображение',
);

$this->menu=array(
	array('label'=>'Медиа-архив', 'url'=>array('admin')),
	// array('label'=>'Создать видео', 'url'=>array('createVideo')),
	array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);

$this->pageTitle = "Добавить изображение в альбом {$album->name}";
?>

<?php $this->renderPartial('_picture_form', array('model'=>$model)); ?>