<?php
/* @var $this MediaController */
/* @var $model Media */
/* @var $form CActiveForm */
?>
<script>
	filesCount = 0;
</script>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'media-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'	
	),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>
	
	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?= $form->labelEx($model, 'image'); ?>
		<?php $this->widget('CMultiFileUpload', array(
			'model'=>$model,
			'attribute'=>'image',
			'accept'=>'jpg|gif|jpeg|png',
			'options'=>array(
				'afterFileAppend' => 
<<<JS
	function(e, v, m) 
	{
		filesCount++; 
		
		$('div.MultiFile-label:last').append(
			'<br />Заголовок: <input type="text" name="AlbumForm[name][' + filesCount + ']" class="multifile-title"><br /> Описание: <input type="text" name="AlbumForm[description][' + filesCount + ']" class="multifile-description">'
		);
	}
JS
			),
		)); ?>
	</div>
		
	<?= $form->textFieldControlGroup($model->album, 'name', array('class' => 'span8', 'maxLength' => 255)) ?>
	<?= $form->textAreaControlGroup($model->album, 'description', array('class' => 'span8', 'maxlength' => '255')); ?>

	<div class="form-actions">
		<?=	TbHtml::submitButton('Создать', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
	</div>

<?php $this->endWidget(); ?>