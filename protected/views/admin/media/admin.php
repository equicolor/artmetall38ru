<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив'
);

$this->menu=array(
	// array('label'=>'Создать изображение', 'url'=>array('createPicture')),
	// array('label'=>'Создать видео', 'url'=>array('createVideo')),
	array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);

$this->pageTitle = 'Альбомы';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'media-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		// 'content',
		'name',
		// array(
			// 'name' => 'parent_id',
			// 'value' => '$data->albumName',
			// 'filter' => CHtml::listData(
				// Media::model()->albums()->findAll(),
				// 'id',
				// 'id'
			// ),
		// ),
		// 'description',
		'createTime:datetime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
			'updateButtonUrl' => 
				'$data->type != Media::TYPE_ALBUM 
					? 
						Yii::app()->createUrl("admin/media/update", array("id" => $data->id))
					: 
						Yii::app()->createUrl("admin/media/updateAlbum", array("id" => $data->id))',
		),
	),
)); ?>
