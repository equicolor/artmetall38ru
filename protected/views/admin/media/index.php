<?
/**
 * @var $model Face
 */
$this->pageTitle = Yii::t('site', 'Медиа-архив');

?>
<section class="b-content">
	<article class="l-layout">
		<h2 class="b-title b-title--medium"><?= Yii::t('site', 'Медиа-архив') ?></h2>
		<div class="b-media">
			<?php $this->widget('zii.widgets.CListView', array(
				'dataProvider' => $mediaDp,
				'itemView' => '_view',
				'template' => '{items}',
				'enablePagination' => false,
			)); ?>
		</div>
	</article>
</section>