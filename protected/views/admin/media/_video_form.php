<?php
/* @var $this MediaController */
/* @var $model Media */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'media-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'	
	),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>
	
	<?php echo $form->errorSummary($model); ?>
	
	<p class="help-block">Поддерживаются видеохостинги Youtube и Vimeo.</p>
	
	<?= $form->textFieldRow($model, 'content', array('class' => 'span8')); ?>
	
	<?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxLength' => 255)) ?>
	
	<?= $form->textAreaRow($model, 'description', array('rows' => 6, 'class' => 'span8')) ?>

	<?php if (!$model->isNewRecord && !$model->hasErrors('content')): ?>
	
		<?php $this->widget('ext.Yiitube', array('v' => $model->content)); ?>
		
	<?php endif; ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>	
	</div>

<?php $this->endWidget(); ?>
