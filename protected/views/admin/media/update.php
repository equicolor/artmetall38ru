<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив'=>array('admin'),
	'Редактирование'
);

$this->menu=array(
	// array('label'=>'Создать изображение', 'url'=>array('createPicture')),
	// array('label'=>'Медиа-архив', 'url'=>array('admin')),
	// array('label'=>'Создать видео', 'url'=>array('createVideo')),
	// array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);

$this->pageTitle = 'Редактирование изображения'; 
?>

<?php $this->renderPartial('_' . $model->type . '_form', array('model'=>$model)); ?>