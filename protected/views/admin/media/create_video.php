<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs=array(
	'Медиа-архив'=>array('admin'),
	'Создать видео',
);

$this->menu=array(
	array('label'=>'Медиа-архив', 'url'=>array('admin')),
	// array('label'=>'Создать изображение', 'url'=>array('createPicture')),
	array('label'=>'Создать альбом', 'url'=>array('createAlbum')),
);
?>

<h1>Создать видео</h1>

<?php $this->renderPartial('_video_form', array('model'=>$model)); ?>