<div class="b-media--item b-media--item__<?php echo $data->type?>">
    <?php if ($data->type == 'album'): ?>
        <div class="rama" data-fit="cover" data-allowfullscreen="true" data-width="100%" data-maxheight="200">
            <?php foreach ($data->items as $item): ?>
                <a href="<?= $item->content ?>" data-thumb="<?= $item->content->thumb() ?>" data-caption="<?= $item->description?>"></a>
            <?php endforeach ; ?>
        </div>

    <?php elseif ($data->type == 'video'): ?>
        <a href="<?= $data->content?>" class="fancybox b-media--link" target="_blank">
            <img src="<?= $data->thumb; ?>" class="b-media--image"/>
            <?php if($data->description):?><h4 class="b-media--title"><?= $data->description ?></h4><?php endif;?>
        </a>
    <?php endif; ?>
</div>