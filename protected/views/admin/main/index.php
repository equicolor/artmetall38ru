<?php
/* @var $this AdminController */

$this->breadcrumbs=array(
	'Админка',
);

$this->pageTitle = 'Админка';
?>

<?php if (Config::value('dummy')): ?>
	Заглушка включена. <?= CHtml::link('Отключить', array('admin/main/toggleDummy')); ?>
<?php else: ?>
	Заглушка выключена. <?= CHtml::link('Включить', array('admin/main/toggleDummy')); ?>
<?php endif; ?>