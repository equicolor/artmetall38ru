<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Настройки'=>array('admin'),
	'Пароль',
);

$this->menu=array(
	array('label'=>'Создать параметр', 'url'=>array('create', 'file' => 0)),
	array('label'=>'Создать файл', 'url'=>array('create', 'file' => 1)),
	array('label'=>'Список настроек', 'url'=>array('admin')),
);

$this->pageTitle = 'Редактирование пароля';
?>

<?php $this->renderPartial('_password_form', array(
	'model' => $model,
)); ?>