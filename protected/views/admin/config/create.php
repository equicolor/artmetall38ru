<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Настройки'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Создать параметр', 'url'=>array('create', 'file' => 0)),
	array('label'=>'Создать файл', 'url'=>array('create', 'file' => 1)),
	array('label'=>'Список настроек', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать параметр конфигурации';
?>

<?php $this->renderPartial(
	$fileForm ? '_create_file_form' : '_create_string_form', 
	array(
		'model' => $model
	)
); ?>