<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'config-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model, 'id', array('span' => 5, 'maxlength' => 16)); ?>
	
	<?= $form->textFieldControlGroup($model, 'key', array('span' => 5, 'maxlength' => 255)); ?>
	
	<?php if ($model->is_file): ?>
	
	<?= $form->fileFieldControlGroup($model, 'file', array('span' => 5)); ?>
	
	<?= $form->hiddenField($model, 'value', array(
		'value' => $model->value,
	)); ?>
	
	<?php else: ?>
	
	<?= $form->textFieldControlGroup($model, 'value', array('span' => 5, 'maxlength' => 255)); ?>
	
	<?php endif; ?>
	
	<?= $form->hiddenField($model, 'is_file', array(
		'value' => $model->is_file,
	)); ?>

	<div class="form-actions">
		
		<?php if ($model->isNewRecord): ?>
		
		<?=	TbHtml::submitButton('Создать', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
			'name' => 'apply',
		)); ?>
		
		<?php else: ?>
		
		<?=	TbHtml::submitButton('Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
		
		<?=	TbHtml::submitButton('Применить', array(
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
			'name' => 'apply',
		)); ?>
		
		<?php endif; ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->