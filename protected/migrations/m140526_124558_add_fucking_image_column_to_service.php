<?php

class m140526_124558_add_fucking_image_column_to_service extends CDbMigration
{
	public function up()
	{
		$this->addColumn('service', 'image', 'string');
	}

	public function down()
	{
		$this->dropColumn('service', 'image');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}