<?php

class m140520_090435_insert_contact_data_to_config extends CDbMigration
{
	public $rows = array(
		array(
			'id' => 'phone',
			'key' => 'Телефон',
			'value' => '+7 (3952) 000 000',
			'is_file' => 0,
			'is_hidden' => 0,
		),
		array(
			'id' => 'address',
			'key' => 'Адрес',
			'value' => '664000, Иркутск, Партизанская 46А',
			'is_file' => 0,
			'is_hidden' => 0,
		),
		array(
			'id' => 'email',
			'key' => 'E-mail',
			'value' => 'info@artmetall.ru',
			'is_file' => 0,
			'is_hidden' => 0,
		),
	); 

	public function up()
	{
		foreach ($this->rows as $row)
		{
			$this->insert('config', $row);
		}
	}

	public function down()
	{
		foreach ($this->rows as $row)
		{
			$this->delete('config', "id = '{$row['id']}'");
		}
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}