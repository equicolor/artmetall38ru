<?php

class m140526_110352_add_column_image_to_service extends CDbMigration
{
	public function up()
	{
		$this->addColumn('service', 'mediaId', 'int(11)');
	}

	public function down()
	{
		$this->dropColumn('service', 'mediaId');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}