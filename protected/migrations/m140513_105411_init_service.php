<?php

class m140513_105411_init_service extends CDbMigration
{
	public function up()
	{
		$this->createTable('service', array(
			'id' => 'pk',
			'alias' => 'varchar(32) not null',
			'name' => 'string not null',
			'content' => 'mediumtext not null',
			'image' => 'string',
			'position' => 'mediumint',
		));
	}

	public function down()
	{
		$this->dropTable('service');
	}
}