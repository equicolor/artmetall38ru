<?php

class m140429_092225_init_text extends CDbMigration
{
	public function up()
	{
		$this->createTable('text', array(
			'place' => 'varchar(32) NOT NULL PRIMARY KEY',
			'name' => 'string',
			'content' => 'MEDIUMTEXT',
		));
	}

	public function down()
	{
		$this->dropTable('text');
	}
}