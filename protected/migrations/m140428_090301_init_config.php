<?php

class m140428_090301_init_config extends CDbMigration
{
	public function up()
	{
		$this->createTable('config', array(
			'id' => 'varchar(16) NOT NULL PRIMARY KEY',
			'key' => 'string NOT NULL',
			'value' => 'string NOT NULL',
			'is_file' => 'tinyint(1) NOT NULL',
			'is_hidden' => 'tinyint(1) NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('config');
	}
}