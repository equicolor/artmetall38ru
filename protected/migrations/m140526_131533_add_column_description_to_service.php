<?php

class m140526_131533_add_column_description_to_service extends CDbMigration
{
	public function up()
	{
		$this->addColumn('service', 'description', 'text'); 
	}

	public function down()
	{
		$this->dropColumn('service', 'description');
	}
}