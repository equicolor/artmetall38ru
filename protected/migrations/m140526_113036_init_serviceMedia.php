<?php

class m140526_113036_init_serviceMedia extends CDbMigration
{
	public function up()
	{
		$this->createTable('serviceMedia', array(
			'serviceId' => 'int(11) NOT NULL',
			'mediaId' => 'int(11) NOT NULL',
			'PRIMARY KEY(serviceId, mediaId)',
		));
	}

	public function down()
	{
		$this->dropTable('serviceMedia');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}