<?php

class m140526_114307_add_foreign_key_service_has_many_media extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'serviceMediaHasMedia',
			'serviceMedia', 
			'mediaId',
			'media',
			'id',
			'cascade', 'cascade'
		);

		$this->addForeignKey(
			'serviceMediaHasService',
			'serviceMedia',
			'serviceId',
			'service',
			'id',
			'cascade',
			'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('serviceMediaHasMedia', 'serviceMedia');
		$this->dropForeignKey('serviceMediaHasService', 'serviceMedia');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}