<?php

class m140416_104832_media_init extends CDbMigration
{
	public function up()
	{
		$this->createTable('media', array(
			'id' => 'pk',
			'content' => 'text',
			'name' => 'varchar(255)',
			'type' => "enum('picture','video','album')",
			'description' => 'text DEFAULT NULL',
			'createTime' => 'int(11) DEFAULT NULL',
			'updateTime' => 'int(11) DEFAULT NULL',
			'thumb' => 'varchar(255) DEFAULT NULL',
			'parent_id' => 'int(11) DEFAULT NULL',
		));

		$this->addForeignKey(
			'media_media_key',
			'media',
			'parent_id',
			'media', 
			'id',
			'cascade', 'cascade'
		);
	}

	public function down()
	{
		$this->dropTable('media');
	}
}