<?php

class m140526_110335_drop_column_image_in_service extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('service', 'image');
	}

	public function down()
	{
		$this->addColumn('service', 'image', 'string');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}