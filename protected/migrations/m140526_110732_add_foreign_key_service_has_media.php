<?php

class m140526_110732_add_foreign_key_service_has_media extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey(
			'serviceHasMedia',
			'service',
			'mediaId',
			'media',
			'id',
			'cascade', 'cascade'
		);
	}

	public function down()
	{
		$this->dropForeignKey('serviceHasMedia', 'service');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}