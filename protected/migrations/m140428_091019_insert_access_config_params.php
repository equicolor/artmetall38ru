<?php

class m140428_091019_insert_access_config_params extends CDbMigration
{
	public $rows = array(
		array(
			'id' => 'password',
			'key' => 'Пароль',
			'value' => 'bbba7f4b9a703d578333b9ebf3eefa4102fa0b61',
			'is_file' => 0,
			'is_hidden' => 1,
		),
		array(
			'id' => 'salt',
			'key' => 'Соль',
			'value' => 'szp68ubm',
			'is_file' => 0,
			'is_hidden' => 1,
		),
		array(
			'id' => 'login',
			'key' => 'Логин',
			'value' => 'admin',
			'is_file' => 0,
			'is_hidden' => 1,
		),
	);
	                           
	public function up()
	{		
		foreach ($this->rows as $row)
			$this->insert('config', $row);
	}

	public function down()
	{
		$this->delete('config', "id in ('login', 'salt', 'password')");
	}
}