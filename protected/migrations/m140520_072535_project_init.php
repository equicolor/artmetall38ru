<?php

class m140520_072535_project_init extends CDbMigration
{
	public function up()
	{
		$this->createTable('project', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'description' => 'text NOT NULL',
			'content' => 'text NOT NULL',
			'image' => 'string NOT NULL',
			'mediaId' => 'int(11)',
			'position' => 'mediumint',
		));

		$this->addForeignKey(
			'projectHasMedia',
			'project', 
			'mediaId', 
			'media', 
			'id',
			'cascade', 'cascade' 
		);
	}

	public function down()
	{
		$this->dropTable('project');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}