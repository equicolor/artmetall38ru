<?php

/**
 * This is the model class for table "media".
 *
 * The followings are the available columns in table 'media':
 * @property integer $id
 * @property string $content
 * @property string $type
 * @property string $description
 * @property string $name
 * @property integer $createTime
 * @property integer $parent_id
 */
 
class Album extends Media
{
	public function init()
	{
		$this->type = $this->getType();
	}

	public function getTypeName()
	{
		return 'Альбом';
	}
	
	public function getType()
	{
		return Media::TYPE_ALBUM;
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('description, name', 'safe'),
			array('id, content, type, description, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(
			parent::attributeLabels(),
			array(
			
			)
		);
	}
	
	public function resave($data = null)
	{
		if ($data === null)
		{
			$data = array();
			
			foreach ($this->items as $item)
			{
				$data[] = array(
					'id' => $item->id,
				);
			}
			
		}
		
		$this->content = CJSON::encode($data);
		
		return $this->save();
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Album the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className)->albums();
	}
}
