<?php

/**
 * This is the model class for table "media".
 *
 * The followings are the available columns in table 'media':
 * @property integer $id
 * @property string $content
 * @property string $type
 * @property string $description
 * @property string $name
 * @property integer $createTime
 * @property integer $parent_id
 */
class Picture extends Media
{
	public $fakeUploadedFileInstance;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('content', 'required', 'on' => 'insert'),
			array('description, name, parent_id', 'safe'),
			array('id, content, type, description, parent_id', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors()
	{
		return CMap::mergeArray(
			parent::behaviors(),
			array(
				array(
					'class' => 'UploadFileBehavior',
					'attributeNames' => array(
						'content' => array(
							'types' => array('image'),
							'image' => true,
                            'thumbSizes' => array(
                                'default' => array(300, 0),
                            ),
						),
					),	
				),
			)
		);
	}
	
	public function getType()
	{
		return Media::TYPE_PICTURE;
	}

	public function getTypeName()
	{
		return 'Изображение';
	}
	
	public function attributeLabels()
	{
		return CMap::mergeArray(
			parent::attributeLabels(),
			array(
				'content' => 'Изображение',
			)
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Media the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
