<?php

/**
 * This is the model class for table "media".
 *
 * The followings are the available columns in table 'media':
 * @property integer $id
 * @property string $content
 * @property string $type
 * @property string $description
 * @property string $title
 * @property integer $createTime
 * @property integer $parent_id
 */
class Media extends CActiveRecord
{
	const TYPE_PICTURE = 'picture';
	const TYPE_VIDEO = 'video';
	const TYPE_ALBUM = 'album';
	
	public static $factory = array(
		self::TYPE_PICTURE => 'Picture',
		self::TYPE_VIDEO => 'Video',
		self::TYPE_ALBUM => 'Album',
	);
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, content, type, description, name, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'album' => array(self::BELONGS_TO, 'Media', 'parent_id'),
			'items' => array(self::HAS_MANY, 'Media', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Контент',
			'name' => 'Название',
			'type' => 'Тип',
			'description' => 'Описание',
			'createTime' => 'Дата создания',
			'parent_id' => 'Альбом',
		);
	}
	
	public function behaviors()
	{
		return array(
			array(
				'class' => 'AutoTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => null,
			),
		);
	}

	public function scopes()
	{
		return array(
			'albums' => array(
				'condition' => "{$this->tableAlias}.type = :type",
				'params' => array(
					':type' => self::TYPE_ALBUM,
				),
			),
			'exceptPictures' => array(
				'condition' => "{$this->tableAlias}.type != :type",
				'params' => array(
					':type' => self::TYPE_PICTURE,
				),
			),
			'exceptAlbums' => array(
				'condition' => "{$this->tableAlias}.type != :type",
				'params' => array(
					':type' => self::TYPE_ALBUM,
				),
			),
			'pictures' => array(
				'condition' => "{$this->tableAlias}.type == :type",
				'params' => array(
					':type' => self::TYPE_PICTURE,
				),
			),
			'withoutAlbum' => array(
				'condition' => "{$this->tableAlias}.parent_id IS NULL",
			),
		);
	}
	
	public function defaultScope()
	{
		$t = $this->getTableAlias(false, false);
	
		return array(
			'order' => "$t.createTime DESC",
		);
	}
	
	public function attributeScope($attributeName, $value)
	{
		$this->dbCriteria->addCondition("{$this->tableAlias}.$attributeName = :type"); 
		$this->dbCriteria->params[':type'] = $value;		
		
		return $this;
	}
	
	public function type($type)
	{
		return $this->attributeScope('type', $type);
	}
	
	public function parent($parent_id)
	{
		return $this->attributeScope('parent_id', $parent_id);
	}
	
	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
			$this->type = $this->getType();
			
			return true;
		}
		else
			return false;
	}
	
	protected function afterSave()
	{
		parent::afterSave();
		
		$this->updateParent();
	}
	
	protected function afterDelete()
	{
		parent::afterDelete();
		
		$this->updateParent();
	}
	
	protected function updateParent()
	{
		if ($this->album !== null && $this->album instanceOf Album)
		{
			$this->album->resave();
		}
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('createTime',$this->createTime);
		$criteria->compare('parent_id',$this->parent_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Media the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function instantiate($attributes)
	{
		$type = $attributes['type'];
		
		if (!isset(self::$factory[$type]))
			throw new CException('Can\'t instantiate Media: unknown type "' . $type . '"');
		
		$class = self::$factory[$type];
		$model = new $class(null);

		return $model;
	}
	
	public function getAlbumName()
	{
		if ($this->album !== null)
			return $this->album->id;
	}

	public function getNonEmptyTitle() {
		if($this->name) {
			return $this->name;
		} else {
			return $this->id;
		}
	}
}
