<?php
class FeedbackForm extends CFormModel
{
	public $theme;
	public $phone;
	public $email;
	public $body;

	public function rules()
	{
		return array(
			array('theme, phone, email, body', 'required'),
			array('email', 'email'),
			array('body', 'length', 'max' => 1000),
			array('phone', 'length', 'max' => 20),
			array('theme', 'length', 'max' => 100),
		);
	}

	public function attributeLabels()
	{
		return array(
			'phone' => 'Телефон',
			'body' => 'Сообщение',
			'email' => 'E-mail',
			'theme' => 'Тема',
		);
	}

	public function send()
	{
		if ($this->hasErrors())
			return false;

		Yii::import('ext.yiimailer.YiiMailer');

		$mail = new YiiMailer();
			
		$mail->setView('feedback');
		$mail->setData(
			$this->attributes
		);
		$mail->setFrom($this->email, Yii::app()->name);
		$mail->setTo(Config::value('email'));
		$mail->setSubject('Восстановление пароля на сайте ' . Yii::app()->name);	

		return $mail->send();
	}
}