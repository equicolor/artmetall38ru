<?php
class AboutPage extends MMForm 
{
	/* unsafe submodels */
	
	/* safe submodels */
	
	public $text;

	/* unsafe submodel attributes */
	
	/* safe form input attributes */
	
	public $selectedCustomers = array();

	public function rules()
	{
		return array(
			array('selectedCustomers', 'safe'),
			array('text', 'submodel'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'selectedCustomers' => 'Клиенты в сайдбаре',
		);
	}

	public function prepare($params)
	{
		$this->text = $params['text'];
		$selectedCustomers = SelectedCustomer::model()->findAll();

		foreach ($selectedCustomers as $customer)
		{
			$this->selectedCustomers[] = $customer->id;
		}
	}
	
	protected function saveSelectedCustomer($id)
	{
		$model = new SelectedCustomer();
		$model->id = $id;

		return $model->save(false);
	}

	protected function saveSelectedCustomers()
	{
		
		$result = true;

		foreach ($this->selectedCustomers as $selectedCustomer)
		{
			if (!empty($selectedCustomer))
			{
				$result &= $this->saveSelectedCustomer($selectedCustomer);
			}
		}

		return $result;
	}

	public function saveProcess($runValidation = true)
	{
		$result = true;
					
		SelectedCustomer::model()->deleteAll();

		$result &= $this->saveSelectedCustomers();
		$result &= $this->text->save(false);
	
		return $result;
	}

	public function issetAttributes($request)
	{
		return isset($request['Text']);
	}
	
	public function safeAssign($request)
	{
		$this->text->attributes = $request['Text'];

		if (isset($request['AboutPage']))
			$this->attributes = $request['AboutPage'];
	}
	
	public static function create($params, $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
