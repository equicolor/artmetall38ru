<?php
class ProjectForm extends MMForm 
{
	/* safe submodels */
	
	public $album;
	public $project;
	
	/* safe form input attributes */
	
	public $image = array();
	public $description = array();
	public $title = array();
	
	public function rules()
	{
		return array(
			array('album, project', 'submodel'),
			array('images, description, title', 'safe'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'images' => 'Изображения для галереи проекта',
		);
	}

	public function prepare($params)
	{
		if (isset($params['project']))
		{
			$this->project = $params['project'];
			$this->album = $this->project->media;
		}
		else
		{
			$this->project = new Project;
			$this->album = new Album;
		}
	}
	
	public function saveProcess($runValidation = true)
	{
		$result = true;

		$descriptions = array();
		$titles = array();
		$images = array();
		$data = array();
		
		$this->album->description = $this->album->name = $this->album->content = 'Альбом для проекта ' . $this->project->name;
		$result &= $this->album->save();
		
		$this->project->mediaId = $this->album->id;
		$result &= $this->project->save();

		if ($result)
		{
			foreach ($this->description as $description)
				$descriptions[] = $description;

			foreach ($this->title as $title)
				$titles[] = $title;
				
			foreach (CUploadedFile::getInstances($this, 'image') as $key => $instance)
			{
				$image = new Picture;
				$image->fakeUploadedFileInstance = $instance;
				$image->parent_id = $this->album->id;
				
				if (isset($descriptions[$key]))
				{
					$image->description = $descriptions[$key];
				}

				if (isset($titles[$key]))
				{
					$image->title = $titles[$key];
				}
				
				if ($image->save())
				{
					$images[] = $image;
				}
			}
		}
		
		// foreach ($images as $image)
		// {
		// 	$data[] = array(
		// 		'id' => $image->id,
		// 	);
		// }
		
		// $result &= $this->album->resave($data);
		
		return $result;
	}

	
	public function issetAttributes($request)
	{
		return isset($request['Project']);
	}
	
	public function safeAssign($request)
	{
		// $this->album->attributes = $request['Album'];
		$this->project->attributes = $request['Project'];
			
		if (isset($request['AlbumForm']))
			$this->attributes = $request['AlbumForm'];
	}
	
	public static function create($params = array(), $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
