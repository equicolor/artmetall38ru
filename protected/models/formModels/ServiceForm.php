<?php
class ServiceForm extends MMForm 
{
	/* safe submodels */
	
	public $album;
	public $service;
	
	/* safe form input attributes */
	
	public $images = array();
	
	public $albums = array();

	public function rules()
	{
		return array(
			array('album, service', 'submodel'),
			array('images, albums', 'safe'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'images' => 'Изображения для слайдера услуги',
			'albums' => 'Прикрепленные альбомы',
		);
	}

	public function prepare($params)
	{
		$albumExists = false;

		if (isset($params['service']))
		{
			$this->service = $params['service'];
			$this->album = $this->service->media;
			$albumExists = $this->album !== null;
		}
		else
		{
			$this->service = new Service;
		}

		if (!$albumExists)
			$this->album = new Album;

		foreach ($this->service->albums as $album)
		{
			$this->albums[] = $album->id;
		}

		// var_dump($this->albums); die;
	}
	
	public function saveProcess($runValidation = true)
	{
		$result = true;

		$descriptions = array();

		$this->album->description = $this->album->name = $this->album->content = 'Альбом для услуги ' . $this->service->name;
		$result &= $this->album->save();

		$this->service->mediaId = $this->album->id;
		$result &= $this->service->save();

		if ($result)
		{

			foreach (CUploadedFile::getInstances($this, 'images') as $key => $instance)
			{
				$image = new Picture;
				$image->fakeUploadedFileInstance = $instance;
				$image->parent_id = $this->album->id;

				if ($image->save())
				{
					$images[] = $image;
				}
			}
		}

		ServiceMedia::model()->deleteAll(array(
			'condition' => 'serviceId = :serviceId',
			'params' => array(
				':serviceId' => $this->service->id,
			),
		));

		foreach ($this->albums as $albumId)
		{
			$serviceMedia = new ServiceMedia;
			$serviceMedia->serviceId = $this->service->id;
			$serviceMedia->mediaId = $albumId;

			$serviceMedia->save();
		}

		return $result;
	}

	public function issetAttributes($request)
	{
		return isset($request['Service']);
	}
	
	public function safeAssign($request)
	{
		// $this->album->attributes = $request['Album'];
		$this->service->attributes = $request['Service'];
			
		if (isset($request['ServiceForm']))
			$this->attributes = $request['ServiceForm'];
	}
	
	public static function create($params = array(), $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
