<?php
class AlbumForm extends MMForm 
{
	/* safe submodels */
	
	public $album;
	
	/* safe form input attributes */
	
	public $image = array();
	public $description = array();
	public $name = array();

	public function rules()
	{
		return array(
			array('album', 'submodel'),
			array('images, description, name', 'safe'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'entry_id' => 'Новость',
			'request_id' => 'Проект',
			'image' => 'Изображения',
		);
	}

	public function prepare($params)
	{
		if (!isset($params['album']))
		{
			$this->album = new Album;
		}
		else
		{
			$this->album = $params['album'];
				
			$entry = Entry::model()->resetScope()->with('entryMedias')->find(array(
				'condition' => 'entryMedias.media_id = :entryMediaId AND t.user_id IS NULL',
				'params' => array(
				 	':entryMediaId' => $this->album->id,
				),
			));

			if ($entry !== null)
			{
				$entryMedias = $entry->entryMedias;
				$this->oldFakeEntry = $entry;
			}
		}
	}
	
	public function saveProcess($runValidation = true)
	{
		$result = true;

		$descriptions = array();
		$names = array();
		$images = array();
		$data = array();
		
		$result &= $this->album->save();

		if ($result)
		{
			foreach ($this->description as $description)
				$descriptions[] = $description;

			foreach ($this->name as $name)
				$names[] = $name;
				

			foreach (CUploadedFile::getInstances($this, 'image') as $key => $instance)
			{
				$image = new Picture;
				$image->fakeUploadedFileInstance = $instance;
				$image->parent_id = $this->album->id;
				
				if (isset($descriptions[$key]))
				{
					$image->description = $descriptions[$key];
				}

				if (isset($names[$key]))
				{
					$image->name = $names[$key];
				}
				
				if ($image->save())
				{
					$images[] = $image;
				}
			}
		}
		
		foreach ($images as $image)
		{
			$data[] = array(
				'id' => $image->id,
			);
		}
		
		// $result &= $this->album->resave($data);
		return $result;
	}

	
	public function issetAttributes($request)
	{
		return isset($request['Album']);
	}
	
	public function safeAssign($request)
	{
		$this->album->attributes = $request['Album'];

		if (isset($request['AlbumForm']))
			$this->attributes = $request['AlbumForm'];
	}
	
	public static function create($params = array(), $className = __CLASS__)
	{
		return parent::create($params, $className);
	}

}
