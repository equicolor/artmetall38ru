<?
/**
 * @author Фещенко Артём eqcolor@gmail.com
 *
 * Поведение позволяет хранить массив списков строковых пар ключ-значение в JSON в аттрибуте модели
 * Class UploadFileBehavior
 */
class JSONParamsBehavior extends CActiveRecordBehavior
{
	public $listAttribute = 'params';
	public $jsonAttribute = 'json_params';
	public $params = array(
		'key',
		'value',
	);
	
	// param => callback
	public $paramsFilters = array();
	 
	private $_defaultList = null;

	public function attach($owner)
	{
		parent::attach($owner);
		
		$owner->{$this->listAttribute} = $this->defaultList;
		
		$safeValidator = CValidator::createValidator('safe', $owner, $this->jsonAttribute);
		$owner->validatorList->add($safeValidator);
		
		$unsafeValidator = CValidator::createValidator('safe', $owner, $this->listAttribute);
		$owner->validatorList->add($unsafeValidator);
	}
	
	public function beforeSave($event)
	{
		foreach ($this->owner->{$this->listAttribute} as $key => $item)
		{
			if ($this->hasEmptyParams($item))
			{
				unset($this->owner->{$this->listAttribute}[$key]);
			}
			else
			{
				foreach ($item as $param => $value)
				{
					if (isset($this->paramsFilters[$param]))
					{
						//todo: добавить elseif(is_array(...)) и сделать выполнение массива callback-ов
						if (is_callable($this->paramsFilters[$param]))
						{
							$this->owner->{$this->listAttribute}[$key][$param] = call_user_func($this->paramsFilters[$param], $value);
						}
					}
				}
			}
		}
		
		$this->owner->{$this->jsonAttribute} = CJSON::encode($this->owner->{$this->listAttribute});
			
		return true;
	}

	public function beforeValidate($event)
	{
		if (is_array($this->owner->{$this->listAttribute}))
		{
			foreach ($this->owner->{$this->listAttribute} as $key => $item)
			{
				if (!$this->issetParams($item))
				{
					unset($this->owner->{$this->listAttribute}[$key]);
				}
			}
		}
		else
		{
			$this->owner->{$this->listAttribute} = $this->defaultList;
		}
		
		return true;
	}

	public function afterFind($event)
	{
		$list = CJSON::decode($this->owner->{$this->jsonAttribute}, true);
		$this->owner->{$this->listAttribute} = !empty($list) ? $list : $this->defaultList;
	}
	
	protected function hasEmptyParams($item)
	{
		$hasEmpty = false;
		
		foreach ($this->params as $paramName)
		{
			$hasEmpty = $hasEmpty || empty($item[$paramName]);
		}
		
		return $hasEmpty;		
	}
	
	protected function issetParams($item)
	{
		$isset = true;
		
		foreach ($this->params as $paramName)
		{
			$isset = $isset && isset($item[$paramName]);
		}
		
		return $isset;
	}
	
	protected function getDefaultList()
	{
		if ($this->_defaultList === null)
		{
			$item = array();
			foreach ($this->params as $param)
			{
				//todo: '' is bad default value
				// м.б. $this->params = [
				//	param => default value
				//]
				$item[$param] = '';
			}
			$this->_defaultList[] = $item;
		}
		
		return $this->_defaultList;
	}
}