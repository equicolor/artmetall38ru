<?php
return array(
	'hookKey' => 'p8osBlp698nn5T',
	'gitPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '.git',
	'logFile' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'deploy.log',
	'mailsTo' => array(
		'eqcolor@gmail.com',
		'sk00rd@gmail.com',
	),
	'mailFrom' => 'admin@test',
	
	'writeRequestsLog' => false,
	'echoOutput' => true,
);