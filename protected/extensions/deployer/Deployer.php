<?php

Yii::import('ext.yiimailer.YiiMailer');

class Deployer extends CComponent
{
	protected $gitPath;
	protected $logFile;
	protected $branch;
	protected $commits = array();
	protected $mailsTo = array();
	protected $mailFrom;
	protected $echoOutput = false;
	protected $writeRequestsLog = false;
	
	public $output = '';
	
	public function __construct($config, $data)
	{
		$this->gitPath = $config['gitPath'];
		$this->logFile = $config['logFile'];
		$this->mailsTo = $config['mailsTo'];
		$this->mailFrom = $config['mailFrom'];
		$this->writeRequestsLog = $config['writeRequestsLog'];

		$this->commits = $data['commits'];

		if (isset($config['echoOutput']))
		{
			$this->echoOutput = $config['echoOutput'];
		}
		
		$this->branch = $this->currentBranch;
	}
	
	public function deploy()
	{
		$result = false;
		
		if ($this->writeRequestsLog)
		{
			$this->writeRawLog(var_export($_REQUEST, true));
		}

		if ($this->hasCommits($this->branch) || empty($this->commits))
		{
			try 
			{
				if ($this->pull())
				{
					$result = $this->migrate();
				}
			}
			catch (Exception $e)
			{
				$this->handleError($e);
			}
			
			$this->writeLog($result);
		}
		
		return $result;
	}
	
	protected function pull()
	{
		$this->shell('git pull 2>&1', $this->echoOutput);
			
		if (strpos($this->output, 'Automatic merge failed'))
		{
			throw new GitException('Automatic merge failed');
		}
		elseif (strpos($this->output, 'Permission denied'))
		{	
			throw new GitException('Permission denied');
		}
		elseif (strpos($this->output, 'fatal'))
		{
			throw new GitException('Fatal error');
		}
		
		return true;
	}
	
	protected function migrate()
	{
		$this->shell('cd protected/ && php yiic migrate --interactive=0 2>&1', $this->echoOutput);
		
		$success = 'Migrated up successfully';
		$noMigrations = 'No new migration found. Your system is up-to-date';
		
		if (strpos($this->output, $success) || strpos($this->output, $noMigrations)) 
		{
			return true;
		}
		else
		{
			/* TODO: подробности =) */
			throw new MigrationException('Unknown');
		}
		
		return false;
	}
	
	protected function getCurrentBranch()
	{
		$head = file($this->gitPath . DIRECTORY_SEPARATOR . 'HEAD', FILE_USE_INCLUDE_PATH);
		$string = $head[0]; 
		$exploded = explode('/', $string); 
		
		return trim($exploded[2]); 
	}
	
	protected function hasCommits($branch)
	{	
		foreach ($this->commits as $commit)
		{
			if ($commit['branch'] == $branch)
			{
				return true;
			}
		}
		
		return false;
	}
	
	protected function handleError(Exception $e)
	{
		$message = $this->getNotificationMessage($e);
		
		$this->notifyAboutFail($message);
	}
	
	protected function getNotificationMessage(Exception $e)
	{
		$message = Yii::app()->name . ": Deploying failed!\n";
		switch (get_class($e))
		{
			case 'GitException':
				$message .= "Git pull failed with message: {$e->getMessage()}";
				break;
				
			case 'MigrationException':
				$message .= "Migration failed with message: {$e->getMessage()}";
				break;
				
			default:
				$message .= "Unknown fail with message: {$e->getMessage()}";
		}
		
		return $message;
	}
	
	protected function notifyAboutFail($message)
	{
		foreach ($this->mailsTo as $mail)
		{
			$this->mail($mail, $message);
		}
	}
	
	protected function mail($to, $message)
	{
		$mail = new YiiMailer();
		
		$mail->setView('error');
		$mail->setData(array(
			'message' => $message,
		));
		$mail->setFrom($this->mailFrom, Yii::app()->name);
		$mail->setTo($to);
		$mail->setSubject('Deploying failed');	
		$mail->send();
	}
	
	protected function shell($command, $echo = false)
	{
		$output = shell_exec($command);
		
		$this->log($command);
		$this->log($output);		
		
		if ($echo)
		{
			echo $command . "\n";
			echo $output . "\n";
		}
	}
	
	protected function log($st)
	{
		$this->output .= $st . "\n";
	}
	
	protected function writeLog($result)
	{
		/* ugly */
		$output  = "===\n";
		$output .= $result ? 'Deploy success' : 'Deploy failed';
		$output .= "\n";
		$output .= date('d.m.Y H:i:s') . "\n" . $this->output;
		
		file_put_contents($this->logFile, $output, FILE_APPEND);
	}	

	protected function writeRawLog($result)
	{
		$output  = "===\n";
		$output .= $result;
		
		file_put_contents($this->logFile, $output, FILE_APPEND);
	}
}

class GitException extends Exception
{

}

class MigrationException extends Exception
{

}