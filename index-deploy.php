<?php

	ini_set('display_errors', 1);
	error_reporting(E_ALL); 
	
	$config = dirname(__FILE__) . '/protected/config/main.php';

	require(dirname(__FILE__) . '/../yiiframework/YiiBase.php');

	class Yii extends YiiBase
	{
		public static function app()
		{
			return parent::app();
		}
	}
	
	$app = Yii::createWebApplication($config);
	
	$handler = function ()
	{
		Yii::import('application.extensions.deployer.Deployer');
		
		$config = require_once(dirname(__FILE__) . '/protected/config/deploy-config.php');
		
		$data = json_decode(@$_POST['payload'], true);
		
		$e400 = true;
		
		if(isset($data['canon_url'], $data['commits'], $data['repository'], $_GET['key']))
		{
			if ($_GET['key'] == $config['hookKey'])
			{				
				$deployer = new Deployer($config, $data);
				$deployer->deploy();
				$e400 = false;
			}	
		}
		
		if ($e400)
			throw new CHttpException(400);
	};
	
	$app->getEventHandlers('onBeginRequest')->clear();
	$app->attachEventHandler('onBeginRequest', $handler);
	$app->raiseEvent('onBeginRequest', new CEvent);